import TitleImage from "./image/TitleImage";
import TitleText from "./text/TitleText";

function TitleComponent () {
        return (
            <>
                <TitleText />
                <TitleImage />
            </>
        )
}

export default TitleComponent;