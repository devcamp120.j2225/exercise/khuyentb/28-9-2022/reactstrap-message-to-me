import { Col, Row } from "reactstrap";
import background from "../../../assets/images/background.jpg";

function TitleImage () {
        return (
            <Row>
                <Col>
                <img className="img-thumbnail" src={background} alt="title background"/>
                </Col>
            </Row>
        )
}

export default TitleImage;