import { useState } from "react";
import InputMessage from "./form/InputMessage";
import LikeImage from "./result/LikeImage";

function ContentComponent () {
    const [inputMessage, setInputMessage] = useState("");
    const [outputMessage, setOutputMessage] = useState([]);
    const [likeDisplay, setLikeDisplay] = useState(false);
    

    const inputMessageChangeHandler = (value) => {
        setInputMessage(value);
    }

    const outputMessageChangeHandler = () => {
        if(inputMessage) {
        setOutputMessage([...outputMessage, inputMessage]);
            setLikeDisplay(true);
            
        }
    }

    return (
        <>
            <InputMessage inputMessageProps={inputMessage} inputMessageChangeHandlerProps={inputMessageChangeHandler} outputMessageChangeHandlerProps={outputMessageChangeHandler}/>
            <LikeImage outputMessageProps={outputMessage} likeDisplayProps={likeDisplay} />
        </>
    )
}

export default ContentComponent;