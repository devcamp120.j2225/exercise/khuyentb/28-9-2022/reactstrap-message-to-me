import { Col, Input, Label, Row, Button } from "reactstrap";

function InputMessage ({inputMessageProps, inputMessageChangeHandlerProps, outputMessageChangeHandlerProps}) {
    const inputChangeHandler = (event) => {
        let value = event.target.value;
        
        console.log(value);

        inputMessageChangeHandlerProps(value);
    }
    
    const buttonClickHandler = () => {
        console.log("Nút được bấm")

        outputMessageChangeHandlerProps();
    }

    
        return (
            <>
            <Row className="form-floating mt-5">
                <Col>
                    <Label>Message cho bạn 12 tháng tới</Label>
                    <Input type="email" className="form-control" placeholder="Nhập message của bạn" onChange={inputChangeHandler} value={inputMessageProps} ></Input>
                </Col>
            </Row>
            <Button color="success" onClick={buttonClickHandler}>Gửi thông điệp</Button>
            </>
        )
}

export default InputMessage;