import like from "../../../assets/images/like.png";

function LikeImage ({outputMessageProps, likeDisplayProps}) {

    return (
        <>
            {outputMessageProps.map((element, index) => {
                return   <p key={index}>{element}</p>
            })}
        

            { likeDisplayProps ? <img src={like} alt="like" width="100"></img> : <></> } 
        </>
    )
}

export default LikeImage;